package orderedStructures;

public class Arithmetic extends Progression {
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue()  throws IllegalStateException  {
		if(!super.isPassedFV()){
			throw new IllegalStateException("The value used is not the first one. Execute firstValue.");
		}
		current = current + commonDifference; 
		return current;
	}

	public String toString(){		
		return "Arith("+ (int)super.firstValue() +", "+(int)commonDifference+")";
	}
	
	public double getTerm(int n) throws IndexOutOfBoundsException{
		if (n <= 0) 
			throw new IndexOutOfBoundsException("printAllTerms: Invalid argument value = " + n); 
		return (super.firstValue()) + (commonDifference)*(n-1);
		
	}
}
