package orderedStructures;

public class Fibonacci extends Progression {

	private double prev; 
	
	public Fibonacci() { 
		this(1); 
		prev = 0; 
	}
	private Fibonacci(double first) {
		super(first);
	}

	@Override
	public double nextValue()  throws IllegalStateException {
        // add the necessary code here
		if(!super.isPassedFV()){
			throw new IllegalStateException("The value used is not the first one. Execute firstValue.");
		}
		double save=current;
		current=current+prev;
		prev=save;
		return current;
	}
	
	@Override	
	public double firstValue() { 
		double value = super.firstValue(); 
		super.setPassedFV(true);
		prev = 0; 
		return value; 
	}

}
