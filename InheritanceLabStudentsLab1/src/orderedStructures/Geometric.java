package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor;

	public Geometric(double firstValue, double commonFactor) {
		super(firstValue);
		this.commonFactor = commonFactor;
	}

	@Override
	public double nextValue() throws IllegalStateException {
		if(!super.isPassedFV()){
			throw new IllegalStateException("The value used is not the first one. Execute firstValue.");
		}
		current = current * commonFactor;
		return current;
	}

	public String toString() {
		return "Geom(" + (int) super.firstValue() + ", " + (int) commonFactor + ")";
	}
	public double getTerm(int n) throws IndexOutOfBoundsException{	
		if (n <= 0) 
			throw new IndexOutOfBoundsException("printAllTerms: Invalid argument value = " + n); 
		return (super.firstValue()* Math.pow(commonFactor, n-1));
	}
	
}
